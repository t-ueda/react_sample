import React from 'react';
import {render} from 'react-dom';
import Issues from "./issues.jsx";
import Projects from "./projects.jsx";

const host = 'https://cm8tlyypmc.execute-api.ap-northeast-1.amazonaws.com',
      opts = {
          method: 'GET',
          mode: 'cors',
          headers: {
              "Content-Type": "text/plain"
          }
      };

export default class Gitlab extends React.Component {
  constructor(props) {
      super(props);
      this.state = {projectId : ''};
  }

  loadIssues(projectId) {
    console.log(`Gitlab/${projectId}`);
    this.setState({projectId: projectId});
  }

  render() {
    return (
      <div>
        <Projects changeProject={this.loadIssues.bind(this)} />
        <hr/>
        <Issues projectId={this.state.projectId} />
      </div>
    );
  }
}