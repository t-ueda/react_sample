import React from 'react';
import {render} from 'react-dom';

const host = 'https://cm8tlyypmc.execute-api.ap-northeast-1.amazonaws.com',
      opts = {
          mode: 'cors',
          headers: {
              "Content-Type": "text/plain"
          }
      };

export default class Issues extends React.Component {
  constructor(props) {
    super(props);
    this.state =  {issues: [], projectId: props.projectId};
  }

  componentWillReceiveProps(props, state) {
    console.log(props.projectId);
    if(props.projectId) {
      fetch(`${host}/dev/gitlab/issues?projectId=${props.projectId}`, opts).then(response => {
        return response.json();
      }).then(data => {
        this.setState({issues: JSON.parse(data.body).issues});
      }).catch(err => {
        console.log(err);
      });
    }
    return true;
  }

  render() {
    return (
      <ul>
        {this.state.issues.map(issue => {
          return <li key={issue.id}>{issue.title}/{issue.labels}/{issue.state}</li>
        })}
      </ul>
    );
  }
}
