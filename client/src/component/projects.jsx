import React from 'react';
import {render} from 'react-dom';
import PropTypes from 'prop-types';

const host = 'https://cm8tlyypmc.execute-api.ap-northeast-1.amazonaws.com',
      opts = {
          method: 'GET',
          mode: 'cors',
          headers: {
              "Content-Type": "text/plain"
          }
      };
const stateProjects = {projects: []};

export default class Projects extends React.Component {
  constructor(props) {
    super(props);
    this.state = stateProjects;
  }

  componentWillMount() {
    fetch(`${host}/dev/gitlab/projects`, opts).then(response => {
      return response.json();
    }).then(data => {
      console.log(JSON.parse(data.body).projects);
      this.setState({projects: JSON.parse(data.body).projects});
    }).catch(err => {
      console.log(err);
    });
  }

  _changeProject(e) {
    this.props.changeProject(e.target.value);
  }

  render() {
    return (
      <div>
        <select name="project" onChange={this._changeProject.bind(this)}>
          <option value="">選択してください</option>
          {this.state.projects.map(project => {
            return <option value={project.id} key={project.id}>{project.name_with_namespace}</option>
          })}
        </select>
      </div>
    );
  }
}

Projects.propTypes = {
  changeProject: PropTypes.func
}