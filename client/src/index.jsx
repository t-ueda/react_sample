import React from "react";
import { render } from "react-dom";
import Gitlab from "./component/gitlab.jsx";

render(<Gitlab />, document.getElementById("app"));
