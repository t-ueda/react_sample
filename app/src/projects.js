'use strict';
var params = {
    url: 'https://gitlab.com',
    key: process.env.GITLAB_KEY
};

class Projects {
    constructor(token, url) {
        url && (params.url = url);
        token && (params.token = token);
        params.gitlab = require('gitlab')(params);
        this.getProjects = () => {
            return new Promise((resolve, reject) => {
                params.gitlab.projects.all(projects => {
                    resolve(projects);
                });
            });
        };
    }

    projects() {
        return this.getProjects();
    }
}

module.exports = (event, context, callback) => {
    console.log(`event: ${JSON.stringify(event, {}, 2)}`);
    Promise
        .resolve()
        .then((new Projects(params.key)).getProjects)
        .then(projects => {
            console.log(`projects length : ${projects.length}`);
            callback(null, {
                statusCode: 200,
                body: JSON.stringify({
                    size: projects.length,
                    projects: projects
                })
            });
        })
        .catch(err => {
            console.log(err);
        });
};