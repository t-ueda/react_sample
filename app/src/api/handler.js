module.exports = {
    issues: require('../issues'),
    projects: require('../projects'),
    hello: require('../hello'),
    auth: require('../auth')
};