'use strict';

module.exports = (event, context, callback) => {
    callback(null, {
        statusCode: 200,
        body: JSON.stringify({
            say: 'hello'
        })
    });
};