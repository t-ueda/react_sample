'use strict';
var params = {
    url: 'https://gitlab.com',
    key: process.env.GITLAB_KEY
};

class Issues {
    constructor(token, url) {
        url && (params.url = url);
        token && (params.token = token);
        params.gitlab = require('gitlab')(params);
        this.getProjectIssues = projectId => {
            return new Promise((resolve, reject) => {
                params.gitlab.projects.issues.list(projectId, issues => {
                    resolve(issues);
                });
            });
        };
    }

    projectIssues(projectId) {
        return this.getProjectIssues(projectId);
    }
}

module.exports = (event, context, callback) => {
    console.log(`event: ${JSON.stringify(event, {}, 2)}`);
    var data = event.params.querystring;

    Promise
        .resolve(data.projectId)
        .then((new Issues(params.key)).getProjectIssues)
        .then(issues => {
            console.log(`issues length : ${issues.length}`);
            callback(null, {
                statusCode: 200,
                body: JSON.stringify({
                    size: issues.length,
                    issues: issues
                })
            });
        })
        .catch(err => {
            console.log(err);
        });
};