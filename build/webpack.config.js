var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const base = path.resolve(__dirname, '../');
const src = path.resolve(base, 'client/src');
const dist = path.resolve(base, 'client/dist');

module.exports = {
    context: base,
    entry: src + '/index.jsx',
    output: {
        path: dist,
        filename: 'bundle.js'
    },
    module: {
        loaders: [{
            test: /\.jsx?$/,
            loader: 'babel-loader',
            exclude: /node_modules/
        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: src + '/index.html',
            filename: 'index.html'
        })
    ]
};