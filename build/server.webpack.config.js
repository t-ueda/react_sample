var path = require('path');
var nodeExternals = require('webpack-node-externals');
var glob = require('glob');
const base = path.resolve(__dirname, '../');
const targets = glob.sync(`${base}/app/src/api/*.js`);

module.exports = {
    context: base,
    entry: targets,
    target: 'node',
    externals: [nodeExternals()]
};