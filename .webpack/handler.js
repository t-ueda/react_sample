(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = {
	    issues: __webpack_require__(2),
	    projects: __webpack_require__(4),
	    hello: __webpack_require__(5),
	    auth: __webpack_require__(6)
	};

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	var params = {
	    url: 'https://gitlab.com',
	    key: process.env.GITLAB_KEY
	};

	class Issues {
	    constructor(token, url) {
	        url && (params.url = url);
	        token && (params.token = token);
	        params.gitlab = __webpack_require__(3)(params);
	        this.getProjectIssues = projectId => {
	            return new Promise((resolve, reject) => {
	                params.gitlab.projects.issues.list(projectId, issues => {
	                    resolve(issues);
	                });
	            });
	        };
	    }

	    projectIssues(projectId) {
	        return this.getProjectIssues(projectId);
	    }
	}

	module.exports = (event, context, callback) => {
	    console.log(`event: ${JSON.stringify(event, {}, 2)}`);
	    var data = event.params.querystring;

	    Promise
	        .resolve(data.projectId)
	        .then((new Issues(params.key)).getProjectIssues)
	        .then(issues => {
	            console.log(`issues length : ${issues.length}`);
	            callback(null, {
	                statusCode: 200,
	                body: JSON.stringify({
	                    size: issues.length,
	                    issues: issues
	                })
	            });
	        })
	        .catch(err => {
	            console.log(err);
	        });
	};

/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = require("gitlab");

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	var params = {
	    url: 'https://gitlab.com',
	    key: process.env.GITLAB_KEY
	};

	class Projects {
	    constructor(token, url) {
	        url && (params.url = url);
	        token && (params.token = token);
	        params.gitlab = __webpack_require__(3)(params);
	        this.getProjects = () => {
	            return new Promise((resolve, reject) => {
	                params.gitlab.projects.all(projects => {
	                    resolve(projects);
	                });
	            });
	        };
	    }

	    projects() {
	        return this.getProjects();
	    }
	}

	module.exports = (event, context, callback) => {
	    console.log(`event: ${JSON.stringify(event, {}, 2)}`);
	    Promise
	        .resolve()
	        .then((new Projects(params.key)).getProjects)
	        .then(projects => {
	            console.log(`projects length : ${projects.length}`);
	            callback(null, {
	                statusCode: 200,
	                body: JSON.stringify({
	                    size: projects.length,
	                    projects: projects
	                })
	            });
	        })
	        .catch(err => {
	            console.log(err);
	        });
	};

/***/ },
/* 5 */
/***/ function(module, exports) {

	'use strict';

	module.exports = (event, context, callback) => {
	    callback(null, {
	        statusCode: 200,
	        body: JSON.stringify({
	            say: 'hello'
	        })
	    });
	};

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	const aws = __webpack_require__(7);

	var cognitoidentityserviceprovider = new aws.CognitoIdentityServiceProvider({
	    apiVersion: '2016-04-18',
	    region: 'ap-northeast-1'
	});

	module.exports = (event, context, callback) => {
	    const token = event.authorizationToken != null ? event.authorizationToken : event.headers.Authorization
	    var params = {
	        AccessToken: token
	    };
	    cognitoidentityserviceprovider.getUser(params, function (err, data) {
	        if (err) {
	            console.log(err)
	            callback(null, generatePolicy('user', 'Deny', event.methodArn, token));
	        } else {
	            console.log(data)
	            callback(null, generatePolicy('user', 'Allow', event.methodArn, token));
	        }
	        return;
	    });
	};

	//"methodArn":"arn:aws:execute-api:<regionId>:<accountId>:<apiId>/<stage>/<method>/<resourcePath>"
	const generatePolicy = function generatePolicy(principalId, effect, resource, token) {
	    return {
	        principalId: principalId,
	        policyDocument: {
	            Version: '2012-10-17',
	            Statement: [{
	                Action: 'execute-api:Invoke',
	                Effect: effect,
	                Resource: "arn:aws:execute-api:ap-northeast-1:*:cm8tlyypmc/*"
	                //CHECK!! =>  http://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/permissions.html#api-gateway-calling-api-permissions
	            }]
	        }
	    };
	};

/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = require("aws-sdk");

/***/ }
/******/ ])));