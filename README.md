# React + Serverlessの勉強で作った残骸

## 何してるの？
Gitlabから選択したProjectのIssuesを取ってくるだけのSPA

## 前提
- 環境変数
```
GITLAB_KEY=gitlabのAccessToken
```

- AWSのCredentials
```
export AWS_ACCESS_KEY_ID=<your-key-here>
export AWS_SECRET_ACCESS_KEY=<your-secret-key-here>
```

## 構成
- サーバサイド（ServerlessFramework）  
app/src以下にコードを置いて、apiフォルダにlambda登録に使うhundler.jsを配置。  
各APIは個別に作りつつ、apiのhundler.jsでロードしてmodule.exportsに登録している。

- クライアント（React）  
client/src以下にhtmlとjsxを配置。  
babelとwebpackでトランスパイル＆パッキングしてclient/distにコピー。  
AWS S3へのアップロードは手動。